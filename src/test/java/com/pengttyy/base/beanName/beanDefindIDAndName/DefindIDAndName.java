package com.pengttyy.base.beanName.beanDefindIDAndName;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by KaiPeng on 2016/6/9.
 */
public class DefindIDAndName extends SpringUnitTestBase {
    @Test
    public void testDefindIDAndName() throws Exception {
        testDefindByName("myBeanID");
        testDefindByName("myBeanName");
        testDefindByName("myBeanName2");
    }

    private void testDefindByName(String myBeanName) {
        String beanByName = this.context.getBean(myBeanName, String.class);
        assertEquals("hello spring", beanByName);
    }
}
