package com.pengttyy.base.beanName.beanDefindNotID;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by KaiPeng on 2016/6/9.
 */
public class NotIDTest extends SpringUnitTestBase {
    public NotIDTest() {
        super("com/pengttyy/base/beanName/beanDefindNotID/notId.xml");
    }

    @Test
    public void testNotId() throws Exception {
        String bean = getBean(String.class);
        assertEquals("hello spring", bean);
    }
}
