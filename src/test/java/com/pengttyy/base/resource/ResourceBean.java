package com.pengttyy.base.resource;

import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;

/**
 * ResourceLoaderAware使用
 * Created by wseng9 on 2016/6/27.
 */
public class ResourceBean implements ResourceLoaderAware {
    private ResourceLoader resourceLoader;

    ResourceLoader getResourceLoader() {
        return resourceLoader;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
}
