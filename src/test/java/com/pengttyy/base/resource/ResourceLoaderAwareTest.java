package com.pengttyy.base.resource;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;
import org.springframework.core.io.ResourceLoader;

import static org.junit.Assert.assertEquals;

/**
 * ResourceLoaderAware接口test
 * Created by wseng9 on 2016/6/27.
 */
public class ResourceLoaderAwareTest extends SpringUnitTestBase {
    @Test
    public void resource_when_aware() throws Exception {
        ResourceBean resourceBean = this.getBean("resource_bean", ResourceBean.class);
        ResourceLoader resourceLoader = resourceBean.getResourceLoader();
        assertEquals(resourceLoader, this.context);

    }
}
