package com.pengttyy.base.resource;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.*;

/**
 * ResourceLoader接口
 * Created by wseng9 on 2016/6/27.
 */
public class ResourceTest {
    @Test
    public void use_then_defalut_resource_loader() throws Exception {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource("classpath:com/pengttyy/base/resource/test.txt");
        Assert.assertTrue(resource instanceof ClassPathResource);

        //加载文件
        Resource fileResource = resourceLoader.getResource("file:com/pengttyy/base/resource/test.txt");
        Assert.assertTrue(fileResource instanceof UrlResource);

        //根据上下文决定加载
        Resource defaultResource = resourceLoader.getResource("com/pengttyy/base/resource/test.txt");
        Assert.assertTrue(defaultResource instanceof ClassPathResource);
    }


}
