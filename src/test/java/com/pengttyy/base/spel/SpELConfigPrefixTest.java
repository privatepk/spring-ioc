package com.pengttyy.base.spel;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * 自定义spel的前缀和后缀，详情查看SpELConfigPrefix.xml
 * Created by KaiPeng on 2016/7/9.
 */
public class SpELConfigPrefixTest extends SpringUnitTestBase {
    @Test
    public void configPrefix() throws Exception {
        String bean = this.context.getBean("configPrefix", String.class);
        assertEquals("5", bean);
    }
}
