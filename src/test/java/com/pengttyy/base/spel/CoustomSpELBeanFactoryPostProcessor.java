package com.pengttyy.base.spel;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanExpressionResolver;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.expression.StandardBeanExpressionResolver;

/**
 * BeanFactoryPostProcessor
 * IoC容器创建好但还未进行任何Bean初始化时被ApplicationContext实现调用
 * 在此处理发spel前后缀
 * Created by KaiPeng on 2016/7/9.
 */
public class CoustomSpELBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        BeanExpressionResolver resolver = beanFactory.getBeanExpressionResolver();
        StandardBeanExpressionResolver standardResolver = (StandardBeanExpressionResolver) resolver;
        standardResolver.setExpressionPrefix("${");
        standardResolver.setExpressionSuffix("}");
    }
}
