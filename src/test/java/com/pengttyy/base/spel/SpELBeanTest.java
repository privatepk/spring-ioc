package com.pengttyy.base.spel;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.Properties;

import static org.junit.Assert.assertNotNull;

/**
 * 引用Bean
 * Created by KaiPeng on 2016/7/8.
 */
public class SpELBeanTest extends SpringUnitTestBase {
    @Test
    public void bean_expression() throws Exception {
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setBeanResolver(new BeanFactoryResolver(this.context));
        Properties properties = parser.parseExpression("@systemProperties").getValue(context, Properties.class);
        assertNotNull(properties);
    }
}
