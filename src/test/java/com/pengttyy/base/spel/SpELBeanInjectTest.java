package com.pengttyy.base.spel;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * 在xml中使用spel表达式
 * Created by KaiPeng on 2016/7/9.
 */
public class SpELBeanInjectTest extends SpringUnitTestBase {
    @Test
    public void use_xml_spel() throws Exception {
        String hello = this.context.getBean("hello_ref_world", String.class);
        assertEquals("5", hello);
    }


}
