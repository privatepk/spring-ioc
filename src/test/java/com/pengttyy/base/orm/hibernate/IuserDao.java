package com.pengttyy.base.orm.hibernate;

/**
 * Created by wseng9 on 2016/7/13.
 */
public interface IuserDao {
    void save(User user);

    void update(User user);

    User get(int id);

    void delete(int id);
}
