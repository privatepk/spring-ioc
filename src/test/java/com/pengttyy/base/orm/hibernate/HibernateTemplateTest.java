package com.pengttyy.base.orm.hibernate;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;
import org.springframework.orm.hibernate4.HibernateTemplate;

import java.io.Serializable;

/**
 * Created by wseng9 on 2016/7/13.
 */
public class HibernateTemplateTest extends SpringUnitTestBase {
    private HibernateTemplate template;

    @Override
    public void before() throws Exception {
        super.before();
        this.template = this.context.getBean(HibernateTemplate.class);
    }

    @Test
    public void use_hibernate_template() throws Exception {
        User user = new User();
        user.setName("xxxxxx");

        /// TODO: 2016-07-13,0013 这里其实是为了让保存不报异常，实际需要手式来控制事务
        //this.template.setCheckWriteOperations(false);
        Serializable save = this.template.save(user);

    }
}
