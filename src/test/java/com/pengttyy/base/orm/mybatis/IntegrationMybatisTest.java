package com.pengttyy.base.orm.mybatis;

import com.pengttyy.base.SpringUnitTestBase;
import com.pengttyy.base.orm.hibernate.IuserDao;
import com.pengttyy.base.orm.hibernate.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.mybatis.spring.SqlSessionTemplate;

import javax.sql.DataSource;

/**
 * Created by wseng9 on 2016/7/14.
 */
public class IntegrationMybatisTest extends SpringUnitTestBase {
    @Test
    public void use_datasource() throws Exception {
        DataSource bean = this.context.getBean(DataSource.class);
        System.out.println(bean);
    }

    @Test
    public void get_sql_session_factory_bean() throws Exception {
        SqlSessionFactory factoryBean = this.context.getBean(SqlSessionFactory.class);
        SqlSession session = factoryBean.openSession();
        User user = getUser();
        int insert = session.insert("User.insert", user);
        Assert.assertEquals(1, insert);
    }

    @Test
    public void get_sql_template() throws Exception {
        SqlSessionTemplate template = this.context.getBean(SqlSessionTemplate.class);
        User user = getUser();
        int insert = template.insert("User.insert", user);
        Assert.assertEquals(1, insert);
    }

    private User getUser() {
        User user = new User();
        user.setName("pk");
        return user;
    }

    @Test
    public void user_dao_support() throws Exception {
        IuserDao dao = this.context.getBean(IuserDao.class);
        dao.save(getUser());
    }
}
