package com.pengttyy.base.orm.mybatis;

import com.pengttyy.base.orm.hibernate.IuserDao;
import com.pengttyy.base.orm.hibernate.User;
import org.mybatis.spring.support.SqlSessionDaoSupport;

/**
 * Created by KaiPeng on 2016-07-14,0014.
 */
public class MybatisUserDaoImpl extends SqlSessionDaoSupport implements IuserDao {
    @Override
    public void save(User user) {
        this.getSqlSession().insert("User.insert", user);
    }

    @Override
    public void update(User user) {

    }

    @Override
    public User get(int id) {
        return null;
    }

    @Override
    public void delete(int id) {

    }
}
