package com.pengttyy.base.transaction;

import com.pengttyy.base.orm.hibernate.User;

/**
 * Created by KaiPeng on 2016-07-17,0017.
 */
public interface IuserService {
    void saveUser(User user);

    void saveUserThrow(User user) throws Exception;
}
