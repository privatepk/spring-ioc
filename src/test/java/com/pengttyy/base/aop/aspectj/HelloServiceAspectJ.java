package com.pengttyy.base.aop.aspectj;

/**
 * Created by KaiPeng on 2016/7/9.
 */
public class HelloServiceAspectJ implements IHelloAspectJ {

    @Override
    public void sayHelloBefore() {
        System.out.println("sayHelloBefore");
    }

    @Override
    public String sayHelloAfterReturning() {
        System.out.println("sayHelloAfterReturning");
        return "ok";
    }

    @Override
    public void sayHelloAfterThrowing(String myParam) throws Exception {
        System.out.println("sayHelloAfterThrowing");
        throw new Exception("这是指定的异常");
    }
}
