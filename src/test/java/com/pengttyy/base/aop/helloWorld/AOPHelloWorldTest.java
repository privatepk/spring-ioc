package com.pengttyy.base.aop.helloWorld;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

/**
 * Created by KaiPeng on 2016/7/9.
 */
public class AOPHelloWorldTest extends SpringUnitTestBase {
    @Test
    public void aop_hello_world() throws Exception {
        IHello bean = this.context.getBean("aop_hello_world", IHello.class);
        bean.sayHello();
    }

    @Test
    public void aop_hello_world2() throws Exception {
        IHello bean = this.context.getBean("aop_hello_world", IHello.class);
        bean.sayHello2();
    }

    @Test
    public void aop_hello_world3() throws Exception {
        IHello bean = this.context.getBean("aop_hello_world", IHello.class);
        bean.sayHello("TEST");
    }

    @Test
    public void aop_hello_world_afterreturn() throws Exception {
        IHello bean = this.context.getBean("aop_hello_world", IHello.class);
        boolean flag = bean.sayHelloReturn();
    }

    @Test(expected = RuntimeException.class)
    public void aop_hello_world_afterthrow() throws Exception {
        IHello bean = this.context.getBean("aop_hello_world", IHello.class);
        boolean flag = bean.sayHelloafterThrowing();
    }

    @Test
    public void aop_hello_world_around() throws Exception {
        IHello bean = this.context.getBean("aop_hello_world", IHello.class);
        bean.sayHelloAround("pk");
    }

    /**
     * 引入
     *
     * @throws Exception
     */
    @Test
    public void aop_hello_world_import() throws Exception {
        IIntroduction bean = this.context.getBean("aop_hello_world", IIntroduction.class);
        bean.induct();
//        IHello bean1 = (IHello) bean;
//        bean1.sayHello();
    }
}
