package com.pengttyy.base.aop.order;

/**
 * Created by KaiPeng on 2016-07-10,0010.
 */
public interface IHelloOrder {

    void sayHelloWorld();
}
