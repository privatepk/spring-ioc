package com.pengttyy.base.instantiation.constructor;

/**
 * Created by KaiPeng on 2016/6/10.
 */
public class InjectByInstanceFactory {

    public static Person newStaticInstance(String name, int age, String phone) {
        return new Person(name, age, phone);
    }

    public Person newInstance(String name, int age, String phone) {
        return new Person(name, age, phone);
    }

}
