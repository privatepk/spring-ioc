package com.pengttyy.base.instantiation.constructor;

/**
 * Created by KaiPeng on 2016/6/10.
 */
public class Person {
    private String name;
    private int age;
    private String phone;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, String phone) {
        this.name = name;
        this.age = age;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", phone='" + phone + '\'' +
                '}';
    }
}
