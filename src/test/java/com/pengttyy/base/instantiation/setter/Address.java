package com.pengttyy.base.instantiation.setter;

/**
 * Created by KaiPeng on 2016/6/10.
 */
public class Address {
    private String detailName;

    public String getDetailName() {
        return detailName;
    }

    public void setDetailName(String detailName) {
        this.detailName = detailName;
    }

    @Override
    public String toString() {
        return "Address{" +
                "detailName='" + detailName + '\'' +
                '}';
    }
}
