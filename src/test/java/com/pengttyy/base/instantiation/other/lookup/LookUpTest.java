package com.pengttyy.base.instantiation.other.lookup;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * 场景:在单例类中使用原型类，由于单例类会在实例化是注入原型类，所以在原型类的生命周期变为跟单例类一样
 * 如果需要在单例使用原型，并且使用spring的ioc的话，则可以使用lookup 方法注入来解决
 * Created by wseng9 on 2016/6/23.
 */
public class LookUpTest extends SpringUnitTestBase {
    @Test
    public void singleton_use_prototype_then_prototype_init() throws Exception {
        Singletion singletionObj = this.getBean("singletonBean", Singletion.class);
        Prototype prototypeObj = singletionObj.getPrototype();
        Prototype prototypeObj2 = singletionObj.getPrototype();
        assertTrue(prototypeObj == prototypeObj2);
    }

    @Test
    public void singleton_use_prototype_then_lookup() throws Exception {
        Singletion singletionObj = this.getBean("singletonBeanLookup", Singletion.class);
        Prototype prototypeObj = singletionObj.getPrototype();
        Prototype prototypeObj2 = singletionObj.getPrototype();
        assertTrue(prototypeObj != prototypeObj2);
        assertFalse(singletionObj.getClass() == Singletion.class);
    }

    @Test
    public void use_then_method_replacer() throws Exception {
        Singletion singletionObj = this.getBean("methodReplacerBean", Singletion.class);
        String result = singletionObj.targetReplaceMethod();
        System.out.println(result);
        System.out.println(singletionObj.getClass());
    }
}
