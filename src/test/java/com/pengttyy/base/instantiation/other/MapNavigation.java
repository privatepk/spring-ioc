package com.pengttyy.base.instantiation.other;

import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by KaiPeng on 2016/6/12.
 */
public class MapNavigation {
    private String[] arrStr;
    private List<String> list;
    private Map<String, String> map;
    private Properties properties;

    public MapNavigation() {
        this.arrStr = new String[3];
    }

    public String[] getArrStr() {
        return arrStr;
    }

    public void setArrStr(String[] arrStr) {
        this.arrStr = arrStr;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
