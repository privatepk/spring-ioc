package com.pengttyy.base.instantiation.other;

/**
 * Created by wseng9 on 2016/6/23.
 */
public class SonResourceObj {
    public void init() {
        System.out.println(this.getClass().getName() + "init....");
    }

    public void destroy() {
        System.out.println(this.getClass().getName() + "destroy....");
    }
}
