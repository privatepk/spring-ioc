package com.pengttyy.base.instantiation.other.lookup;

import org.springframework.beans.factory.support.MethodReplacer;

import java.lang.reflect.Method;

/**
 * 方法替换需要实现MethodReplacer接口
 * Created by wseng9 on 2016/6/23.
 */
public class MyMethodReplacer implements MethodReplacer {
    @Override
    public Object reimplement(Object o, Method method, Object[] objects) throws Throwable {
        System.out.println("开始替换");
        //Object invoke = method.invoke(o, objects);
        System.out.println("替换完成");
        return this.getClass().getSimpleName();
    }
}
