package com.pengttyy.base.instantiation.other;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wseng9 on 2016/6/23.
 * 延迟实例化
 */
public class LazyInitTest extends SpringUnitTestBase {

    @Test
    public void use_when_lazy_init() throws Exception {
        String lazyBean = this.getBean("lazyBean", String.class);
        Assert.assertEquals("hello world!", lazyBean);
    }

    @Test
    public void use_when_depends_on() throws Exception {
        ResourceObj resource = this.getBean("depends_on_bean", ResourceObj.class);
        resource.execute();

        /*com.pengttyy.base.instantiation.other.SonResourceObjinit....
        com.pengttyy.base.instantiation.other.ResourceObjinit....
        执行一些操作
        com.pengttyy.base.instantiation.other.ResourceObjdestroy....
        com.pengttyy.base.instantiation.other.SonResourceObjdestroy....*/
    }
}
