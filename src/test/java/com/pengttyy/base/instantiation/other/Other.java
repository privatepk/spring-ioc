package com.pengttyy.base.instantiation.other;

/**
 * Created by KaiPeng on 2016/6/11.
 */
public class Other {
    private String refBean;

    public Other() {
    }

    public Other(String refBean) {
        this.refBean = refBean;
    }

    public String getRefBean() {
        return refBean;
    }

    public void setRefBean(String refBean) {
        this.refBean = refBean;
    }
}
