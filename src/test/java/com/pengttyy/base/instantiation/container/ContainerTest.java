package com.pengttyy.base.instantiation.container;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by KaiPeng on 2016/6/11.
 */
public class ContainerTest extends SpringUnitTestBase {
    @Test
    public void use_when_list() throws Exception {
        CollectionStub stub = getCollectionStub("use_when_list");
        int size = stub.getNames().size();
        assertEquals(3, size);
    }

    private CollectionStub getCollectionStub(String use_when_list) {
        return this.getBean(use_when_list, CollectionStub.class);
    }

    @Test
    public void use_when_set() throws Exception {
        CollectionStub stub = getCollectionStub("use_when_set");
        int size = stub.getAlias().size();
        assertEquals(3, size);
    }

    @Test
    public void use_when_array() throws Exception {
        CollectionStub stub = getCollectionStub("use_when_array");
        int size = stub.getArray().length;
        assertEquals(3, size);
    }

    @Test
    public void use_when_map() throws Exception {
        CollectionStub stub = getCollectionStub("use_when_map");
        int size = stub.getMap().size();
        assertEquals(3, size);
    }

    @Test
    public void use_when_properties() throws Exception {
        CollectionStub stub = getCollectionStub("use_when_properties");
        int size = stub.getProp().size();
        assertEquals(3, size);

    }
}
