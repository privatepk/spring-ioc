package com.pengttyy.base.instantiation.container;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Created by KaiPeng on 2016/6/11.
 */
public class CollectionStub {
    private List<String> names;
    private Set<String> alias;
    private String[] array;
    private Map<String, String> map;
    private Properties prop;

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public Set<String> getAlias() {
        return alias;
    }

    public void setAlias(Set<String> alias) {
        this.alias = alias;
    }

    @Override
    public String toString() {
        return "CollectionStub{" +
                "names=" + names +
                ", alias=" + alias +
                '}';
    }

    public String[] getArray() {
        return array;
    }

    public void setArray(String[] array) {
        this.array = array;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public Properties getProp() {
        return prop;
    }

    public void setProp(Properties prop) {
        this.prop = prop;
    }
}
