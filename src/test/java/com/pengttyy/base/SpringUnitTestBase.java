package com.pengttyy.base;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.StringUtils;

@RunWith(BlockJUnit4ClassRunner.class)
public class SpringUnitTestBase {

    protected ClassPathXmlApplicationContext context;

    private String springXmlpath;

    public SpringUnitTestBase() {
    }

    public SpringUnitTestBase(String springXmlpath) {
        this.springXmlpath = springXmlpath;
    }


    @Before
    public void before() throws Exception {
        if (StringUtils.isEmpty(springXmlpath)) {
            springXmlpath = this.getClass().getName().replaceAll("Test$", "").replaceAll("\\.", "/") + ".xml";
        }
        context = new ClassPathXmlApplicationContext(springXmlpath.split("[,\\s]+"));
        context.start();
    }

    @After
    public void after() throws Exception {
        context.destroy();
    }

    @SuppressWarnings("unchecked")
    protected <T extends Object> T getBean(String beanId) {
        return (T) context.getBean(beanId);
    }

    protected <T extends Object> T getBean(Class<T> clazz) {
        return context.getBean(clazz);
    }

    protected <T extends Object> T getBean(String beanName, Class<T> clazz) {
        return context.getBean(beanName, clazz);
    }

}
